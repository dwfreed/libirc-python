import asyncio
import inspect
import functools


class Connection(asyncio.Protocol):
    def __init__(self, host, port, nick, user, realname, password="", ssl=False, **kwargs):
        self.host = host
        self.port = port
        self.nick = nick
        self.user = user
        self.realname = realname
        self.password = password
        self.ssl = ssl
        self.transport = None
        self.hooks = {}
        for key in kwargs:
            setattr(self, key, kwargs[key])
        self.attach_hooks_from_decorators(self)

    def connect(self):
        loop = asyncio.get_event_loop()
        create_connection = functools.partial(loop.create_connection, lambda: self)
        signature = inspect.signature(create_connection)
        kwargs = {}
        for parameter in signature.parameters:
            if hasattr(self, parameter):
                kwargs[parameter] = getattr(self, parameter)
        coro = create_connection(**kwargs)
        asyncio.ensure_future(coro)

    def _run_hooks(self, hook, *args, **kwargs):
        lists_of_functions = self.hooks.get(hook, ([], [], []))
        for functions in lists_of_functions:
            for function in functions:
                result = function(*args, **kwargs)
                if result is False:
                    return False
        return True

    def connection_made(self, transport):
        self.transport = transport
        self._run_hooks("connection made")

    def connection_lost(self, exception):
        self._run_hooks("connection lost", exception)

    def data_received(self, data):
        print(data)

    def eof_received(self):
        self._run_hooks("eof received")

    def add_hook_first(self, hook, function):
        if hook not in self.hooks:
            self.hooks[hook] = ([], [], [])
        lists_of_functions = self.hooks[hook]
        lists_of_functions[0].insert(0, function)

    def add_hook(self, hook, function):
        if hook not in self.hooks:
            self.hooks[hook] = ([], [], [])
        lists_of_functions = self.hooks[hook]
        lists_of_functions[1].append(function)

    def add_hook_last(self, hook, function):
        if hook not in self.hooks:
            self.hooks[hook] = ([], [], [])
        lists_of_functions = self.hooks[hook]
        lists_of_functions[2].append(function)

    def remove_hook(self, hook, function):
        if hook in self.hooks:
            for functions in self.hooks[hook]:
                if function in functions:
                    functions.remove(function)
                    return True
        return False

    def hook(self, hook):
        def wrapped(function):
            self.add_hook(hook, function)
            return function

        return wrapped

    def hook_first(self, hook):
        def wrapped(function):
            self.add_hook_first(hook, function)
            return function

        return wrapped

    def hook_last(self, hook):
        def wrapped(function):
            self.add_hook_last(hook, function)
            return function

        return wrapped

    def attach_hooks_from_decorators(self, instance):
        methods = inspect.getmembers(instance, predicate=inspect.ismethod)
        for (method_name, method) in methods:
            for hook_type in ('hook_first', 'hook', 'hook_last'):
                if hasattr(method, hook_type):
                    for hook in getattr(method, hook_type):
                        setattr(instance, method_name, getattr(self, hook_type)(hook)(method))
